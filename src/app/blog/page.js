'use client';
import React, { useEffect } from 'react';
import Link from 'next/link';
import { useSelector } from 'react-redux';
export default function page() {
  const { blog } = useSelector((state) => state.blog);

  useEffect(() => {
    console.log(blog);
  }, [blog]);
  return (
    <>
      <h3>Blog</h3>
      <Link href="/dashboard">Dashboard</Link>
    </>
  );
}
