'use client';
import React from 'react';
import { Layout } from 'antd';
const { Content } = Layout;
import './index.css';
import SideBar from '@/components/organism/Sidebar';
import HeaderCustom from '@/components/organism/Header';
import DashboardContent from './DashboardContent';
export default function page() {
  return (
    <>
      <DashboardContent />
    </>
  );
}
