import React from 'react';
import { Card, Col, Row, Table, Typography } from 'antd';
const { Title, Text, Link } = Typography;
import './index.css';
import Chart from "react-apexcharts";
import { ArrowRightOutlined, LeftOutlined, RightOutlined } from '@ant-design/icons';
import ScheduleBox from '@/components/atoms/ScheduleBox';
import Image from 'next/image';
export default function DashboardContent() {
  const dataSource = [
    {
      key: '1',
      name: 'Frontend Developer',
      image:"company1.svg",
      company:"Telkom",
      age: 32,
      address: '10 Downing Street',
      type:"Full Time",
    },
    {
      key: '2',
      name: 'Backend Developer',
      image:"assets/company1.svg",
      company:"Google",
      type:"Freelance",
      age: 42,
      address: '10 Downing Street',
    },
  ];

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render:(_,data)=>{
        return <Image src={"/assets/"+data.image} width={64} height={64}/>
      }
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render:(_,data)=>{
        return (
          <>
          <Title level={5}>{data.name}</Title>
          <Text>{data.company} <span>&middot;</span> {data.type}</Text>
          </>
        )
      }
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
  ];
  const options = {
    colors:['#E9EBFD', '#4640DE',],
    legend:{
      show:false
    }
  }
  const series = [40,60]
  return (
    <div>
      <div>
        <Title level={4}>Good morning, Jake</Title>
        <Text>
          Here is what’s happening with your job search applications from July 19 - July 25.
        </Text>
      </div>
      <Row gutter={24}>
        <Col span={6} style={{ display: 'grid', gap: 20 }}>
          <Card style={{ position: 'relative' }}>
            <Title level={5} style={{ fontSize: 20 }}>
              Total Jobs Applied
            </Title>
            <Text style={{ fontSize: 72, fontWeight: 'bold' }}>45</Text>
            <div
              className="icon-card"
              style={{ background: 'url(/assets/icon1.svg)', backgroundRepeat: 'no-repeat' }}></div>
          </Card>
          <Card style={{ position: 'relative' }}>
            <Title level={5} style={{ fontSize: 20 }}>
              Interviewed
            </Title>
            <Text style={{ fontSize: 72, fontWeight: 'bold' }}>18</Text>
            <div
              className="icon-card"
              style={{ background: 'url(/assets/icon2.svg)', backgroundRepeat: 'no-repeat' }}></div>
          </Card>
        </Col>
        <Col span={8}>
          <Card>
            <Title level={5} style={{ fontSize: 20 }}>
              Jobs Applied Status
            </Title>
            <div className="chart-box">
              <div className='chart'>
              <Chart
              options={options}
              series={series}
              labels={[40,60]}
              type="donut"

            />
              </div>
            
            <div className='series'>
              <div className='series-data'>
                <div className='series-rect' style={{background:"#4640DE"}}></div>
                <div>
                  <Title level={5}>60%</Title>
                  <Text>Unsuitable</Text>
                </div>
              </div>
              <div className='series-data'>
                <div className='series-rect' style={{background:"#E9EBFD"}}></div>
                <div>
                  <Title level={5}>60%</Title>
                  <Text>Unsuitable</Text>
                </div>
              </div>
            </div>
            </div>
            <Link className='link-strong'>View All Applications <ArrowRightOutlined/> </Link>
          </Card>
        </Col>
        <Col span={8}>
          <Card>
            <Title level={5} style={{ fontSize: 20 }}>
              Jobs Applied Status
            </Title>
            <div className="schedul-header">
              <p><strong>Today,</strong> 26 November</p>
              <div className='moving-schedule'>
                <LeftOutlined/>
                <RightOutlined/>
              </div>
            </div>
            <div className="schedul-body">
              <ScheduleBox/>
              <ScheduleBox/>
            </div>
            <Link className='link-strong'>View All Applications</Link>
          </Card>
        </Col>
      </Row>
      <Card
        title="Default size card"
        extra={<a href="#">More</a>}
        style={{ width: '100%', marginTop: 32 }}>
        <Table dataSource={dataSource} columns={columns} showHeader={false} />
      </Card>
    </div>
  );
}
