'use client';
import React from 'react';
import { Epilogue } from 'next/font/google';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import StyledComponentsRegistry from './lib/AntdRegistry';
import { store, persistor } from '@/redux';
import { Layout } from 'antd';
const { Content } = Layout;
import SideBar from '@/components/organism/Sidebar';
import HeaderCustom from '@/components/organism/Header';

import './globals.css';
import { ConfigProvider } from 'antd';
import theme from '@/utils/themeConfig';
const font = Epilogue({ subsets: ['latin'] });

const RootLayout = ({ children }) => {
  const segment = children.props.childProp.segment;
  console.log(segment);
  const path_except = ['', 'login', 'signup', 'job'];
  const is_except = path_except.findIndex((el) => el == segment);

  return (
    <html lang="en">
      <body className={font.className}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <StyledComponentsRegistry>
              <ConfigProvider theme={theme}>
                {is_except !== -1 ? (
                  children
                ) : (
                  <Layout className="layout-wrapper">
                    <SideBar />
                    <Layout>
                      <HeaderCustom />
                      <Content className="content">{children}</Content>
                    </Layout>
                  </Layout>
                )}
              </ConfigProvider>
            </StyledComponentsRegistry>
          </PersistGate>
        </Provider>
      </body>
    </html>
  );
};

export default RootLayout;
