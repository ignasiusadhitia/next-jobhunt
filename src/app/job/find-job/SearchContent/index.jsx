'use client';
import React from 'react';
import bgSeach from '@/assets/image/bg-jobsearch.png';
import './index.css';
import { Typography } from 'antd';
import FormSearch from '@/components/organism/FormSearch';
const { Title, Text } = Typography;
export default function SearchContent() {
  return (
    <div className="search-content" style={{ background: `url(/assets/image/bg-jobsearch.png)` }}>
      <Title level={2} style={{ textAlign: 'center' }}>
        Find Your
        <span style={{ color: '#26A4FF', paddingLeft: 16, position: 'relative' }}>
          dream companies<div className="title-bottom"></div>
        </span>
      </Title>
      <div style={{ textAlign: 'center', marginBottom: 24 }}>
        <Text>Find your next career at companies like HubSpot, Nike, and Dropbox</Text>
      </div>

      <FormSearch />
    </div>
  );
}
