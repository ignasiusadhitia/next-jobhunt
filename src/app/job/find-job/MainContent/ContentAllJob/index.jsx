'use client';
import React, { useEffect } from 'react';
import { Pagination } from 'antd';
import CardJob from '@/components/organism/CardJob';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetJob } from '@/redux/job/action';

export default function ContentAllJob() {
  const dispatch = useDispatch();
  const { job } = useSelector((state) => state.job);
  useEffect(() => {
    dispatch(actionGetJob());
    console.log(job);
  }, []);
  return (
    <>
      {job.map((data, index) => {
        return <CardJob data={data} key={index} />;
      })}

      <Pagination defaultCurrent={1} total={50} style={{ textAlign: 'center' }} />
    </>
  );
}
