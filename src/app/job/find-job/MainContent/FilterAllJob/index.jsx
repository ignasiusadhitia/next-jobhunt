import React from 'react';
import { Row, Col, Typography, Select } from 'antd';
const { Title, Text } = Typography;
import { AppstoreOutlined, MenuOutlined } from '@ant-design/icons';
export default function FilterAllJob() {
  return (
    <Row>
      <Col span={8}>
        <Title level={3} style={{ margin: 0 }}>
          All Jobs
        </Title>
        <Text>Showing 73 results</Text>
      </Col>
      <Col
        span={8}
        offset={8}
        style={{ display: 'flex', gap: 22, padding: 14, alignItems: 'center' }}>
        <div>
          <Text>Sort By :</Text>
          <Select
            defaultValue="most"
            options={[
              { value: 'most', label: 'Most relevant' },
              { value: 'lucy', label: 'Lucy' },
              { value: 'Yiminghe', label: 'yiminghe' },
              { value: 'disabled', label: 'Disabled', disabled: true },
            ]}
          />
        </div>

        <div style={{ width: 1, background: '#D6DDEB' }}></div>
        <AppstoreOutlined style={{ fontSize: 24 }} />
        <MenuOutlined style={{ fontSize: 24 }} />
      </Col>
    </Row>
  );
}
