'use client';
import { Row, Col, Typography, Select } from 'antd';
import React from 'react';
import FilterAllJob from './FilterAllJob';
import ContentAllJob from './ContentAllJob';
import FilterCategory from './FilterCategory';

export default function MainContent() {
  return (
    <div>
      <Row gutter={[40, 40]} style={{ marginTop: 72, marginLeft: 123, marginRight: 123 }}>
        <Col span={6}>
          <FilterCategory />
        </Col>
        <Col span={18}>
          <FilterAllJob />
          <ContentAllJob />
        </Col>
      </Row>
    </div>
  );
}
