import {  serviceGetJob,serviceGetType,serviceGetCategory,serviceGetLevel,serviceGetSalary } from "../../service/job";
export const actionGetJob = () => async (dispatch)=>{
    dispatch({
        type:'job_loading',
        payload:{
            loading:true
        }
    })
    const {data} = await serviceGetJob();
    
    dispatch({
        type:'job_success',
        payload:{
            job:data,
            loading:false
        }
    })
}
export const actionGetFilter = () => async (dispatch)=>{
    try{
        
        dispatch({
            type:'job_loading',
            payload:{
                loading:true
            }
        })
        const resType = await serviceGetType();
        const resCategory = await serviceGetCategory();
        const resLevel = await serviceGetLevel();
        const resSalary = await serviceGetSalary();
        
        dispatch({
            type:'job_type_success',
            payload:{
                jobType:resType.data,
                jobCategory:resCategory.data,
                jobLevel:resLevel.data,
                jobSalary:resSalary.data,
                loading:false
            }
        })
    }catch(err){
        console.log(err)
    }
    
}