import React, { useState } from 'react';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import './index.css';
export default function Collapse({ items, defaultKey }) {
  const [showChildren, setShowChildren] = useState(defaultKey);
  const clickCollapse = (key) => {
    const isFind = showChildren.includes(key);

    if (isFind) {
      console.log('isfind');
      let newarray = showChildren.filter((element) => element !== key);
      setShowChildren(newarray);
    } else {
      setShowChildren([...showChildren, key]);
    }
  };

  return (
    <>
      {items.map((data) => {
        const isFind = showChildren.includes(data.key);
        return (
          <div>
            <div className="collapse-header" onClick={() => clickCollapse(data.key)}>
              {data.label}
              <div className="collapse-icon">{isFind ? <DownOutlined /> : <UpOutlined />}</div>
            </div>
            <div
              className="collapse-children"
              style={isFind ? { display: 'block' } : { display: 'none' }}>
              {data.children}
            </div>
          </div>
        );
      })}
    </>
  );
}
