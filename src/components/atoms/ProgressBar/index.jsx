import React from 'react';
import './index.css';
export default function ProgressBar({ size }) {
  return (
    <div className="progress-wrap">
      <div className="progress" style={{ width: `${size}% ` }}></div>
    </div>
  );
}
