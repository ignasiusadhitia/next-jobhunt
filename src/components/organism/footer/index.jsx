'use client';
import React from 'react';
import { Row, Col, Typography, Input, Button, Space } from 'antd';
import logo from '@/assets/image/logo.png';
const { Text, Title } = Typography;
import {
  FacebookOutlined,
  InstagramOutlined,
  DribbbleOutlined,
  LinkedinOutlined,
  TwitterOutlined,
} from '@ant-design/icons';
import './index.css';
export default function FooterTemplate() {
  return (
    <div className="footer">
      <Row
        style={{
          marginTop: 72,
          paddingTop: 64,
          paddingLeft: 124,
          paddingRight: 124,
          marginLeft: 0,
          marginRight: 0,
        }}
        gutter={[50, 0]}>
        <Col span={8}>
          <div className="logo">
            <img src={logo} />
          </div>
          <Text style={{ color: '#D6DDEB' }}>
            Great platform for the job seeker that passionate about startups. Find your dream job
            easier.
          </Text>
        </Col>
        <Col span={4}>
          <Title level={5} style={{ color: '#FFFFFF' }}>
            About
          </Title>
          <ul className="list-footer">
            <li>Companies</li>
            <li>Pricing</li>
            <li>Terms</li>
            <li>Advice</li>
            <li>Privacy Policy</li>
          </ul>
        </Col>
        <Col span={4}>
          <Title level={5} style={{ color: '#FFFFFF' }}>
            About
          </Title>
          <ul className="list-footer">
            <li>Companies</li>
            <li>Pricing</li>
            <li>Terms</li>
            <li>Advice</li>
            <li>Privacy Policy</li>
          </ul>
        </Col>
        <Col span={6}>
          <Title level={5} style={{ color: '#FFFFFF' }}>
            Get job notifications
          </Title>
          <Text style={{ color: '#D6DDEB' }}>
            The latest job news, articles, sent to your inbox weekly.
          </Text>
          <Space direction="horizontal">
            <Input size="large" />
            <Button type="primary">Subscribe</Button>
          </Space>
        </Col>
      </Row>
      <Row
        style={{
          marginLeft: 0,
          marginRight: 0,
          marginTop: 80,
          borderTop: 'solid 1px rgb(255,255,255,0.2)',
          paddingTop: 64,
          paddingLeft: 124,
          paddingRight: 124,
        }}
        gutter={[50, 0]}>
        <Col span={8}>2021 @ JobHuntly. All rights reserved.</Col>
        <Col span={8} offset={8} className="footer-icon">
          <div className="icon-box-footer">
            <FacebookOutlined />
          </div>
          <div className="icon-box-footer">
            <InstagramOutlined />
          </div>
          <div className="icon-box-footer">
            <DribbbleOutlined />
          </div>
          <div className="icon-box-footer">
            <LinkedinOutlined />
          </div>
          <div className="icon-box-footer">
            <TwitterOutlined />
          </div>
        </Col>
      </Row>
    </div>
  );
}
