'use client';
import React, { useEffect } from 'react';
import { Layout, Typography, Button } from 'antd';
const { Header } = Layout;
const { Title } = Typography;
import './index.css';
import { BellOutlined } from '@ant-design/icons';
import { usePathname } from 'next/navigation';
import { menuList } from '@/utils/menuConfig';
import { useDispatch } from 'react-redux';
import { actionGetUser } from '@/redux/user/action';
export default function HeaderCustom() {
  const pathname = usePathname();
  const findMenu = menuList.find((prev) => prev.route == pathname);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actionGetUser());
  }, []);
  return (
    <Header className="header">
      <div>
        <Title level={3}>{findMenu && findMenu.label}</Title>
      </div>
      <div style={{ display: 'flex', gap: 20 }}>
        <Button>Back to Homepage</Button>
        <BellOutlined style={{ fontSize: 24 }} />
      </div>
    </Header>
  );
}
