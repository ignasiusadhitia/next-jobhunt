import React from 'react';
import { Card, Typography, Tag, Button, Row, Col } from 'antd';
import ProgressBar from '@/components/atoms/ProgressBar';
const { Text, Title } = Typography;
export default function CardJob({ data, key }) {
  return (
    <Card key={key} style={{ marginBottom: 16 }}>
      <Row>
        <Col span={2}>
          <img src={data.logo} width={64} height={64} />
        </Col>
        <Col span={18}>
          <Title level={5}>{data.title}</Title>
          <Text>Agency &middot; {data.location}</Text>
          <div style={{ display: 'flex', gap: 8 }}>
            <Tag color="success">{data.type}</Tag>
            <div style={{ width: 1, background: '#D6DDEB' }}></div>
            <Tag color="warning">{data.category}</Tag>
          </div>
        </Col>
        <Col span={4}>
          <Button type="primary" style={{ borderRadius: 0, width: '100%', marginBottom: 15 }}>
            Apply
          </Button>
          <ProgressBar size={60} />
          <Text style={{ fontSize: 14 }}>5 applied of 10 capacity</Text>
        </Col>
      </Row>
    </Card>
  );
}
