import React from 'react';
import { Button, Input, Select } from 'antd';
import { SearchOutlined, EnvironmentOutlined } from '@ant-design/icons';
import './index.css';
export default function FormSearch() {
  return (
    <div className="form-search">
      <div className="search-group">
        <SearchOutlined style={{ fontSize: 24 }} />
        <Input placeholder="Job title or keyword" className="input-search" />
      </div>
      <div className="search-group">
        <EnvironmentOutlined style={{ fontSize: 24 }} />
        <Select
          defaultValue="lucy"
          className="input-search"
          options={[
            { value: 'jack', label: 'Jack' },
            { value: 'lucy', label: 'Lucy' },
            { value: 'Yiminghe', label: 'yiminghe' },
            { value: 'disabled', label: 'Disabled', disabled: true },
          ]}
        />
      </div>

      <Button type="primary">Search</Button>
    </div>
  );
}
