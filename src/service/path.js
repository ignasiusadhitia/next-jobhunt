const base_url = 'http://localhost:8000';

export const register_path = base_url + '/register';
export const signin_path = base_url + '/signin';
export const profile_path = base_url + '/profile';

export const get_blog = base_url + '/';
export const get_detail_blog = base_url + '/detail';

export const jobPath = `${base_url}/api/job`;
export const jobTypePath = `${base_url}/api/job/type`;
export const jobCategoryPath = `${base_url}/api/job/category`;
export const jobLevelPath = `${base_url}/api/job/level`;
export const jobSalaryPath = `${base_url}/api/job/range-salary`;
