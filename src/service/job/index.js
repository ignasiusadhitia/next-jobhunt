import axios from 'axios'
import { jobCategoryPath, jobLevelPath, jobPath,jobSalaryPath,jobTypePath } from '../path'
export const serviceGetJob = async ()=> {
    return await axios.post(jobPath)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}
export const serviceGetType = async ()=> {
    return await axios.post(jobTypePath)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}
export const serviceGetCategory = async ()=> {
    return await axios.post(jobCategoryPath)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}
export const serviceGetLevel = async ()=> {
    return await axios.post(jobLevelPath)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}
export const serviceGetSalary = async ()=> {
    return await axios.post(jobSalaryPath)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}

